import React from 'react';

function App() {
    const encodeQueryData = data => {
        const ret = [];
        for (let d in data)
            ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
        return ret.join('&');
    };

    const signInWithApple = () => {
        const data = {
            client_id: '[CLIENT ID]',
            redirect_uri: '[REDIRECT ID]',
            response_type: 'code id_token',
            response_mode: 'form_post', // form_post, query, fragment
            scope: 'name email'
        };
        const querystring = encodeQueryData(data);

        window.location.href =
            'https://appleid.apple.com/auth/authorize?' + querystring;
    };

    return (
        <div>
            <header>
                <button
                    style={{ width: '150px', height: '75px' }}
                    onClick={signInWithApple}
                >
                    sign in wth apple
                </button>
            </header>
        </div>
    );
}

export default App;
