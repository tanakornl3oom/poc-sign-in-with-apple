import React, { Fragment, useEffect } from 'react';

const Redirect = props => {
    useEffect(() => {
        console.log(props.location);
        console.log(props.location.hash);
    }, [props.location]);

    return (
        <Fragment>
            redirect
            <br />
            {props.location.hash}
        </Fragment>
    );
};

export default Redirect;
